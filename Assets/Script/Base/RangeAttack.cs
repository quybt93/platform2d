﻿using UnityEngine;
using System.Collections;
using Unity.VisualScripting;

public class RangeAttack : MonoBehaviour 
{
    
    public GameObject Projectile;
    public int damage;
    public Transform firePoint;
    public void Fire()
    {
        SpawnSystemHelper.GetNextObject(Projectile, firePoint.transform.position, firePoint.transform.rotation, true);
    }
}
