﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttack : MonoBehaviour 
{
	public LayerMask CollisionMask;
	public int damage;
	public float force = 15f;
	public Transform MeleePoint;
	public float attackZone = .7f;

	public GameObject hitFX;

	public void Attack()
	{
		var hits = Physics2D.CircleCastAll(MeleePoint.position, attackZone, Vector2.zero,0,CollisionMask);

		if (hits == null)
			return;
		foreach (var hit in hits)
		{
			var obj = (ICanTakeDamage) hit.collider.gameObject.GetComponent (typeof(ICanTakeDamage));
			if (obj == null)
				continue;

            Vector2 pushForce = (hit.transform.position - transform.position).normalized * force;
            obj.TakeDamage(damage, pushForce);
            
            if (hitFX)
				Instantiate (hitFX, hit.point, hitFX.transform.rotation);

			break;
		}
    }

	void OnDrawGizmos(){
		if (MeleePoint == null)
			return;
		
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere (MeleePoint.position, attackZone);
	}
}
