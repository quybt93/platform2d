using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityController : MonoBehaviour, ICanTakeDamage
{
    protected Rigidbody2D rb;
    public bool isFacingRight = true;
    protected Animator anim;
    public Transform centerPoint;
    public LayerMask wallLayer;
    public LayerMask groundLayer;
    public HPController hpController;
    protected bool isHurt;
    protected bool isDead;
    public MeleeAttack meleeAttack { get; set; }
    public RangeAttack rangeAttack { get; set; }

    [Header("Properties")]
    public float speed = 3f;
    public float hp = 10;
    public int damage = 1;

    protected virtual void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponentInChildren<Animator>();
        meleeAttack = GetComponentInChildren<MeleeAttack>();
        rangeAttack = GetComponentInChildren<RangeAttack>();
        if (meleeAttack != null)
            meleeAttack.damage = damage;
        if (rangeAttack != null)
            rangeAttack.damage = damage;
    }
    protected virtual void Flip()
    {
        transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
        isFacingRight = transform.localScale.x > 0;
    }
    public void MeleeAttack()
    {
        if (meleeAttack != null)
        {
            anim.SetTrigger("meleeAttack");
        }
    }
    public void RangeAttack()
    {
        if (rangeAttack != null)
        {
            anim.SetTrigger("rangeAttack");
        }
    }
    protected virtual void OnDie()
    {
        anim.SetTrigger("isDead");
        isDead = true;
    }

    public void TakeDamage(int damage, Vector2 force)
    {
        isHurt = true;
        anim.SetTrigger("isHurt");
        Invoke(nameof(stopHurt), 0.2f);
        hpController.TakeDamage(damage);
        rb.velocity = new Vector2(force.x, rb.velocity.y + force.y);
    }
    protected virtual void stopHurt()
    {
        isHurt = false;
    }
}
