using UnityEngine;

public class EnemyController : EntityController
{
    [SerializeField] float force = 15f;
    Vector2 direction = new Vector2(1, 0);
    private void OnEnable()
    {
        hpController.onDie = OnDie;
        hpController.HP = hp;
    }
    private void Update()
    {
        if (IsWalled() || IsFall())
        {
            speed = -speed;
            Flip();
        }
    }
    void FixedUpdate()
    {
        if (!isHurt || !isDead)
        {
            rb.velocity = new Vector2(speed, rb.velocity.y);
        }
    }
    protected override void Flip()
    {
        base.Flip();
        direction = new Vector2(-direction.x, direction.y);
    }
    bool IsWalled()
    {
        return Physics2D.Raycast(centerPoint.position, direction, 1f, wallLayer);
    }
    bool IsFall()
    {
        return !Physics2D.Raycast(centerPoint.position,  direction + new Vector2(0, -1), 3f, groundLayer);
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Vector3 direction3D = new Vector3(direction.x, direction.y, 0f);
        Gizmos.DrawLine(centerPoint.position, centerPoint.position + direction3D);
        Gizmos.DrawLine(centerPoint.position, (centerPoint.position + direction3D + new Vector3(0, -1, 0)*3f));
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent(typeof(ICanTakeDamage)))
        {
            Vector2 pushForce = (collision.transform.position - transform.position).normalized * force;
            collision.gameObject.GetComponent<ICanTakeDamage>().TakeDamage(damage, pushForce);
        }
    }
    protected override void OnDie()
    {
        base.OnDie();
        Destroy(gameObject);
    }
}
