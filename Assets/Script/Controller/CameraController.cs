using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    Transform target;
    Vector3 velocity = Vector3.zero;
    [Range(0, 1)]
    public float smoothTime = 0.5f;

    [Header("Axis Limitation")]
    public Vector2 xLimit;
    public Vector2 yLimit;
    Camera _camera;
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        _camera = GetComponent<Camera>();
    }
    void Update()
    {
        Vector3 targetPos = target.position;
        float cameraHeight = _camera.orthographicSize;
        float cameraWidth = _camera.orthographicSize * _camera.aspect;
        targetPos = new Vector3(Mathf.Clamp(targetPos.x, xLimit.x + cameraWidth, xLimit.y - cameraWidth), Mathf.Clamp(targetPos.y, yLimit.x + cameraHeight, yLimit.y - cameraHeight), -10);
        transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref velocity, smoothTime);
    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Vector2 boxSize = new Vector2(xLimit.y - xLimit.x, yLimit.y - yLimit.x);
        Vector2 center = (new Vector2(xLimit.y + xLimit.x, yLimit.y + yLimit.x)) * 0.5f;
        Gizmos.DrawWireCube(center, boxSize);

    }
}
