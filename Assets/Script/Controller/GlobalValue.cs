using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalValue : MonoBehaviour
{
    static int levelPlaying = 1;
    public static int Coin
    {
        get { return PlayerPrefs.GetInt("Coin", 0); }
        set
        {
            PlayerPrefs.SetInt("Coin", value);
        }
    }
    public static int LevelPlaying
    {
        get { return levelPlaying; }
        set
        {
            int newValue = Mathf.Clamp(value, 1, LevelHighest);
            levelPlaying = newValue;
        }
    }
    public static int LevelHighest
    {
        get { return PlayerPrefs.GetInt("LevelHighest", 1); }
        set
        {
            int newValue = Mathf.Clamp(value, 1, 2);
            PlayerPrefs.SetInt("LevelHighest", newValue);
        }
    }
}
