using UnityEngine;

public class PlayerController : EntityController
{
    public Vector2 input;
    public int coin;
    bool isPlaying;
    [Header("Jump")]
    [SerializeField] float jumpForce = 17f;
    [SerializeField] float fallMulti = 3f;
    Vector2 vecGravity;
    [SerializeField] float jumpTime = 0.4f;
    [SerializeField] float jumpMulti = 3f;
    float jumpCounter;
    bool isJumping;
    public int numberOfJumpMax = 2;
    int numberOfJump;
    [HideInInspector]
    public bool jumpDown;

    [Header("WallSlide")]
    [SerializeField] float wallSildingSpeed = 1f;
    bool isWallJumping;
    [SerializeField] bool isEasy;
    float wallJumpDirection;
    [SerializeField] float wallJumpTime = 0.4f;
    float wallJumpCounter;
    [SerializeField] float wallJumpForce = 16f;

    void Start()
    {
        hpController = MenuManager.Instance.HealthBar.GetComponentInChildren<HPController>();
        hpController.onDie = OnDie;
        hpController.HP = hp;
        isPlaying = MenuManager.Instance.gameState;
        vecGravity = new Vector2(0, -Physics2D.gravity.y);
        numberOfJump = numberOfJumpMax;
    }
    private void Update()
    {
        if (!isPlaying)
            return;
        HandleInput();
        HandleAnimation();
        WallSlide();

        if (IsGrounded())
        {
            numberOfJump = numberOfJumpMax;
        }

        if (rb.velocity.y > 0)
        {
            if (isJumping)
            {
                jumpCounter += Time.deltaTime;
                if (jumpCounter > jumpTime)
                    isJumping = false;
                float t = jumpCounter / jumpTime;
                float currentJumpMulti = jumpMulti;
                if (t > 0.5f)
                {
                    currentJumpMulti = jumpMulti * (1 - t);
                }
                rb.velocity += vecGravity * currentJumpMulti * Time.deltaTime;
            }
            if (isWallJumping)
            {
                wallJumpCounter += Time.deltaTime;
                if (wallJumpCounter > wallJumpTime)
                    isWallJumping = false;
            }
        }

        if (rb.velocity.y < 0)
        {
            rb.velocity -= vecGravity * fallMulti * Time.deltaTime;
        }
    }

    private void FixedUpdate()
    {
        if (!isPlaying)
            return;
        if (isWallJumping || isHurt || isDead)
            return;
        rb.velocity = new Vector2(input.x * speed, rb.velocity.y);

    }
    private void HandleInput()
    {
        if (isDead)
            return;
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            MoveLeft();
        else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            MoveRight();
        else if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.RightArrow))
            StopMove();

        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
            FallDown();
        if (Input.GetKeyDown(KeyCode.Space))
            Jump();
        if (Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.DownArrow) || Input.GetKeyUp(KeyCode.Space))
            StopMove();
        if (Input.GetKeyDown(KeyCode.J))
            MeleeAttack();
        if (Input.GetKeyDown(KeyCode.K))
            RangeAttack();
    }
    
    bool IsGrounded()
    {
        return Physics2D.OverlapCapsule(transform.position, new Vector2(0.8f, 0.1f), CapsuleDirection2D.Horizontal, 0, groundLayer);
    }

    bool IsWalled()
    {
        return Physics2D.OverlapCapsule(centerPoint.position, new Vector2(1.4f, 2.8f), CapsuleDirection2D.Vertical, 0, wallLayer);
    }

    void WallSlide()
    {
        if (IsWalled() && !IsGrounded() && input.y != -1  )
        {
            rb.velocity = new Vector2(rb.velocity.x, Mathf.Clamp(rb.velocity.y, -wallSildingSpeed, float.MaxValue));
        }
    }
    public void MoveLeft()
    {
        input = new Vector2(-1, 0);
        if (isFacingRight && !isWallJumping)
            Flip();
    }
    public void MoveRight()
    {
        input = new Vector2(1, 0);
        if (!isFacingRight && !isWallJumping)
            Flip();
    }
    public void StopMove()
    {
        input = Vector2.zero;
        isJumping = false;
        if (isEasy)
            isWallJumping = false;
        jumpDown = false;
    }
    public void FallDown()
    {
        input = new Vector2(0, -1);
        jumpDown = true;
    }
    public void Jump()
    {
        if (IsGrounded())
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            isJumping = true;
            jumpCounter = 0;
            numberOfJump = numberOfJumpMax;
        }
        else if (IsWalled())
        {
            isWallJumping = true;
            wallJumpDirection = -transform.localScale.x;
            Flip();
            rb.velocity = new Vector2(wallJumpForce * wallJumpDirection, jumpForce);
            wallJumpCounter = 0;
        }
        else
        {
            numberOfJump--;
            if (numberOfJump > 0)
            {
                rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            }
        }
    }
    void HandleAnimation()
    {
        anim.SetFloat("speed", Mathf.Abs(rb.velocity.x));
        anim.SetBool("isGrounded", IsGrounded());
    }
    protected override void OnDie()
    {
        base.OnDie();
        MenuManager.Instance.GameOver();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        var isTrigger = (ITriggerPlayer)collision.GetComponent(typeof(ITriggerPlayer));
        if (isTrigger != null)
            isTrigger.OnTrigger();
    }
}
