using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HomeMenu : MonoBehaviour
{
    public static HomeMenu Instance;
    public GameObject LoadingUI;

    public void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        LoadingUI.SetActive(false);
    }
    public void LoadScene()
    {
        GlobalValue.LevelPlaying = GlobalValue.LevelHighest;
        LoadingUI.SetActive(true);
        SceneManager.LoadSceneAsync("Level " + GlobalValue.LevelPlaying);
    }
}
