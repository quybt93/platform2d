using System.Collections;
using UnityEngine;

public class PassThroughPlatform : MonoBehaviour
{
    PlayerController playerController;
    PlatformEffector2D effector;
    int originalMask;

    void Start()
    {
        effector = GetComponent<PlatformEffector2D>();
        originalMask = effector.colliderMask;

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        playerController = collision.gameObject.GetComponent<PlayerController>();
    }

    void Update()
    {
        if (playerController != null && playerController.jumpDown)
        {
            effector.colliderMask = effector.colliderMask & ~(1 << LayerMask.NameToLayer("Player"));
            StartCoroutine(EnabledCollider());
        }
    }
    IEnumerator EnabledCollider()
    {
        yield return new WaitForSeconds(0.3f);
        effector.colliderMask = originalMask;
    }
}
