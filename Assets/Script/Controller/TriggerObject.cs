﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerObject : MonoBehaviour
{
	public GameObject[] targets;
	public bool disableTargetOnStart = true;
	List<MonoBehaviour> listMono;
    BoxCollider2D boxCollider2D;


    bool isWorked = false;
	void Awake ()
    {
		Init();
        boxCollider2D = GetComponent<BoxCollider2D>();
    }

	void Init(){

		listMono = new List<MonoBehaviour> ();
        foreach (var obj in targets)
        {
            MonoBehaviour[] monos = obj.GetComponents<MonoBehaviour>();
            foreach (var mono in monos)
            {
                listMono.Add(mono);
                mono.enabled = false;
            }

            obj.SetActive(!disableTargetOnStart);
        }

		isWorked = false;
	}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            if (isWorked)
                return;
            foreach (var obj in targets)
            {
                obj.SetActive(true);
                foreach (var mono in listMono)
                {
                    mono.enabled = true;
                }
            }
            isWorked = true;
        }
    }

    void OnDrawGizmos()
    {
        if (targets != null && targets.Length > 0)
        {
            foreach (var obj in targets)
            {
                if (obj)
                    Gizmos.DrawLine(transform.position, obj.transform.position);
            }
        }
        if (boxCollider2D != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(boxCollider2D.bounds.center, boxCollider2D.bounds.size);
        }
    }
}
