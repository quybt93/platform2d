using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public static MenuManager Instance;
    public GameObject uI, gameOver, finish, LoadingUI, HealthBar;
    public Text txtLevels;
    public Text txtScores;
    int coin;
    public bool gameState;
    private void Awake()
    {
        Instance = this;
        CoinController.CoinAdded += AddCoin;
        gameState= true;
    }
    void Start()
    {
        uI.SetActive(true);
        finish.SetActive(false);
        gameOver.SetActive(false);
        LoadingUI.SetActive(false);
        txtLevels.text  = "Level " + GlobalValue.LevelPlaying;
        txtScores.text = coin.ToString();

    }
    public void Play()
    {
        uI.SetActive(true);
    }
    public void Finish()
    {
        if (GlobalValue.LevelPlaying >= GlobalValue.LevelHighest)
        {
            GlobalValue.LevelHighest++;
        }
        Invoke("FinishCo", 0);
    }

    void FinishCo()
    {
        uI.SetActive(false);
        finish.SetActive(true);
    }

    public void GameOver()
    {
        StopAllCoroutines();
        Invoke("GameOverCo", 0);
    }

    void GameOverCo()
    {
        uI.SetActive(false);
        gameOver.SetActive(true);
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void NextLevel()
    {
        GlobalValue.LevelPlaying++;
        LoadingUI.SetActive(true);
        SceneManager.LoadSceneAsync("Level " + GlobalValue.LevelPlaying);
    }

    public void Home()
    {
        GlobalValue.LevelPlaying = -1;
        LoadingUI.SetActive(true);
        SceneManager.LoadSceneAsync("MainMenu");
    }
    public void AddCoin(int _coin)
    {
        coin += _coin;
        txtScores.text = coin.ToString();
    }
    private void OnDisable()
    {
        CoinController.CoinAdded -= AddCoin;
    }
}
