﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using Unity.VisualScripting;
using UnityEngine;
using static Unity.VisualScripting.Member;

public class Projectile : MonoBehaviour
{
    public int damage = 1;
    public float force = 0;
    public LayerMask targetLayer;
    public float speed = 15f;
    public float maxRange = 20f;
    float range;
    void OnEnable()
    {
        range = maxRange;
    }
    
    private void Update()
    {
        if (speed != 0)
        {
            transform.position += transform.up * (speed * Time.deltaTime);
            range -= speed * Time.deltaTime;
            if (range <= 0)
            {
                DestroyProjectile();
            }
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (targetLayer != (targetLayer | (1 << collision.gameObject.layer)))
            return;
        Vector2 pushForce = (collision.transform.position - transform.position).normalized * force;
        var iCanTakeDame = collision.gameObject.GetComponent<ICanTakeDamage>();
        if (iCanTakeDame != null)
        {
            collision.gameObject.GetComponent<ICanTakeDamage>().TakeDamage(damage, pushForce);
        }
        DestroyProjectile();

    }
    void DestroyProjectile()
    {
        gameObject.SetActive(false);
    }
}

