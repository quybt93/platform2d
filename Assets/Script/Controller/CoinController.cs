using UnityEngine;
using System.Collections;
using System;

public class CoinController : MonoBehaviour, ITriggerPlayer
{
    public int coinToAdd = 1;
    public static event Action<int> CoinAdded;

    public void OnTrigger()
    {
        CoinAdded(coinToAdd);
        Destroy(gameObject);
    }
}
