using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameFinishFlag : MonoBehaviour, ITriggerPlayer
{
    public void OnTrigger()
    {
        MenuManager.Instance.gameState = false;
        MenuManager.Instance.Finish();
        Destroy(this);
    }
}
